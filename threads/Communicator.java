package nachos.threads;

import nachos.machine.*;

/**
 * A communicator allows threads to synchronously exchange 32-bit messages.
 * Multiple threads can be waiting to speak, and multiple threads can be waiting
 * to listen. But there should never be a time when both a speaker and a
 * listener are waiting, because the two threads can be paired off at this
 * point.
 */
public class Communicator {

    /**
     * Allocate a new communicator.
     */
    public Communicator() {
       this.wordSpoken = false;
       this.conditionLock = new Lock();
       this.listenerCondition = new Condition2(conditionLock);
       this.speakerCondition = new Condition2(conditionLock);
       
    }

    /**
     * Wait for a thread to listen through this communicator, and then transfer
     * word to the listener.
     *
     *
     * Does not return until this thread is paired up with a listening thread.
     * Exactly one listener should receive word.
     *
     * @param	word	the integer to transfer.
     */
    public void speak(int word) {
        conditionLock.acquire();
        speakerCount++;
        while (listenerCount == 0 || wordSpoken == true) {
            speakerCondition.sleep();
        }
        this.word1 = word;
        wordSpoken = true;
        listenerCondition.wakeAll();

        speakerCount--;
        conditionLock.release();

    }

    /**
     * Wait for a thread to speak through this communicator, and then return the
     * word that thread passed to speak().
     *
     * @return	the integer transferred.
     */
    public int listen() {

        conditionLock.acquire();
        listenerCount++;

        while (wordSpoken == false) {
            speakerCondition.wakeAll();
            listenerCondition.sleep();
        }
        int w = this.word1;
        wordSpoken = false;
        listenerCount--;
        conditionLock.release();

        return w;
    }

    private int listenerCount = 0;
    private int speakerCount = 0;
    private boolean wordSpoken;
    private Lock conditionLock;
    private int word1 = 0;
    private Condition2 speakerCondition;
    private Condition2 listenerCondition;

}
