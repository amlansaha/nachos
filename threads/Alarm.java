package nachos.threads;

import nachos.machine.*;
import java.util.*;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {

    /**
     * Allocate a new Alarm. Set the machine's timer interrupt handler to this
     * alarm's callback.
     *
     * <p>
     * <b>Note</b>: Nachos will not function correctly with more than one alarm.
     */
    public Alarm() {
        pq = new PriorityQueue<ThreadPriorityQueueNode>(1, new ThreadPQComparator());

        Machine.timer().setInterruptHandler(new Runnable() {
            public void run() {
                timerInterrupt();
            }
        });
    }

    /**
     * The timer interrupt handler. This is called by the machine's timer
     * periodically (approximately every 500 clock ticks). Causes the current
     * thread to yield, forcing a context switch if there is another thread that
     * should be run.
     */
    public void timerInterrupt() {
//	KThread.currentThread().yield();

        boolean intStatus = Machine.interrupt().disable();

//        System.out.println("Timer interrupted at time = " + Machine.timer().getTime());

        while (!pq.isEmpty()) {
            ThreadPriorityQueueNode tpqn = pq.peek();
            if (tpqn.getWakeTime() <= Machine.timer().getTime()) {
                pq.poll();
                tpqn.getThread().ready();
                System.out.println("Waking up: " + tpqn.getThread().getName() + " at time:" + Machine.timer().getTime());
            } else {
                break;
            }
        }

        Machine.interrupt().restore(intStatus);
    }

    /**
     * Put the current thread to sleep for at least <i>x</i> ticks, waking it up
     * in the timer interrupt handler. The thread must be woken up (placed in
     * the scheduler ready set) during the first timer interrupt where
     *
     * <p>
     * <blockquote>
     * (current time) >= (WaitUntil called time)+(x)
     * </blockquote>
     *
     * @param	x	the minimum number of clock ticks to wait.
     *
     * @see	nachos.machine.Timer#getTime()
     */
    public void waitUntil(long x) {
        // for now, cheat just to get something working (busy waiting is bad)
        long wakeTime = Machine.timer().getTime() + x;
//	while (wakeTime > Machine.timer().getTime())
//	    KThread.yield();

        boolean intStatus = Machine.interrupt().disable();
        KThread ct = KThread.currentThread();
        pq.add(new ThreadPriorityQueueNode(ct, wakeTime));

        System.out.println(ct.getName() + " is going to sleep at time: " + (wakeTime - x));
        KThread.sleep();
        Machine.interrupt().restore(intStatus);
    }

    public static void selfTest() {
        System.out.println("******************************");
        System.out.println("Entering Alarm.selfTest()");
        System.out.println("==============================");
        KThread alarmTest1 = new KThread(), alarmTest2 = new KThread();
        alarmTest1.setName("alarmTest1");
        alarmTest2.setName("alarmTest2");
        final Alarm a = new Alarm();

        alarmTest1.setTarget(new Runnable() {

            @Override
            public void run() {
                System.out.println("alarmTest1 is going to wait for 1000 ticks.");
                a.waitUntil(1000);

                System.out.println("alarmTest1 woke up again...");
            }
        });
        alarmTest1.fork();

        alarmTest2.setTarget(new Runnable() {

            @Override
            public void run() {
                System.out.println("alarmTest2 is going to wait for 2000 tics.");
                a.waitUntil(2000);
                System.out.println("alarmTest2 woke up again...");
            }
        });
        alarmTest2.fork();
    }
    private PriorityQueue<ThreadPriorityQueueNode> pq;
}

class ThreadPriorityQueueNode {

    KThread thread;
    Long wake;

    ThreadPriorityQueueNode(KThread kt, Long x) {
        wake = x;
        thread = kt;
    }

    public KThread getThread() {
        return thread;
    }

    public Long getWakeTime() {
        return wake;
    }
}

class ThreadPQComparator implements Comparator<ThreadPriorityQueueNode> {

    @Override
    public int compare(ThreadPriorityQueueNode arg0, ThreadPriorityQueueNode arg1) {
        ThreadPriorityQueueNode node0 = (ThreadPriorityQueueNode) arg0;
        ThreadPriorityQueueNode node1 = (ThreadPriorityQueueNode) arg1;

        if (node0.getWakeTime() < node1.getWakeTime()) {
            return -1;
        } else if (node0.getWakeTime() == node1.getWakeTime()) {
            return 0;
        } else {
            return 1;
        }
    }

}
